<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LogoutController extends Controller
{
    /**
     * Log out account user.
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function perform()
    {
        Session::flush();

        Auth::logout();

        return redirect()->route('login.perform');
    }
    // Auth::logout();

    // request()->session()->invalidate();

    // request()->session()->regenerateToken();

    // return redirect()->route('login/perform');
    // }
}
