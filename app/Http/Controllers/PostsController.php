<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->paginate(10);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file('sampul')->store('sampul'));
        $request->validate([
            'title'              => 'required',
            'description'        => 'required',
            'body'               => 'required',
            'sampul'             => 'image|file||max:1024',
        ]);
        $validatedData = $request->except('sampul');
        if ($request->hasFile('sampul')) {
            $validatedData['sampul'] = $request->file('sampul')->store('sampul');
        }
        $validatedData['user_id'] = auth()->id();
        // dd($validatedData);
        Post::create($validatedData);
        return redirect()->route('posts.index')->with('success', 'Data Berhasil Ditambahkan');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', [
            'post' => $post
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', [
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([

            'title' => 'nullable',
            'description' => 'nullable',
            'body' => 'nullable',
            'sampul' => 'nullable|image|file||max:1024',

        ]);

        if ($request->hasFile('sampul')) {

            $validatedData['sampul'] = $request->file('sampul')->store('sampul');
        }
        Post::findorfail($id)->update($validatedData);
        // dd($validatedData);

        return redirect()->route('posts.index')->with('success', 'Data Berhasil Ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('posts.index')
            ->withSuccess(__('Post deleted successfully.'));
    }
    public function cobabahasa($lang)
    {
        App::setLocale($lang);

        return view('post.index');
    }
}
