<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';
    // protected $guarded = [];
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'body',
        'sampul',
    ];
    public function ambiluser()
    {
        return $this->hasMany(User::class, 'id', 'user_id');
    }
}
