@extends('adminlte.master')

@push('script')
    <script src="../adminlte/plugins/datatables/jquery.dataTables.js"></script>
    <script src="../adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
        });
    </script>
@endpush

@section('content')
    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Postingan</h3>
                <a href="{{ route('posts.create') }}" class="btn btn-primary btn-sm float-right">Add post</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>NAMA</th>
                            <th>DESKRIPSI</th>
                            {{-- <th>BODY</th> --}}
                            <th class="col-1">GAMBAR</th>
                            <th>ACTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($posts as $key => $post)
                            <tr>

                                <td scope="row">{{ $loop->iteration }}</td>
                                <td>{{ $post->title }}</td>
                                <td width="200px">{{ $post->description }}</td>
                                {{-- <td>{{ $post->body }}</td> --}}
                                <td>{{ $post->sampul }}</td>
                                <td>
                                    <a class="btn btn-info btn-sm" href="{{ route('posts.show', $post->id) }}">Show</a>

                                    <a class="btn btn-primary btn-sm" href="{{ route('posts.edit', $post->id) }}">Edit</a>

                                    {!! Form::open(['method' => 'DELETE', 'route' => ['posts.destroy', $post->id], 'style' => 'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
@endsection
