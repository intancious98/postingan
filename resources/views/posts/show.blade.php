<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AdminLTE 3 | Blank Page</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../adminlte/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../adminlte/dist/css/adminlte.min.css">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        @include('adminlte.partials.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('adminlte.partials.sidebar')
        <!-- /.sidebar -->


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            @yield('content')
            <div class="col">

                <div class="card card-primary mt-2">
                    <div class="card-header">
                        <h3 class="card-title">Show Data</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="POST" action="{{ route('posts.update', $post->id) }}">
                        @method('patch')
                        @csrf
                        {{-- <div class="card-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input value="{{ $post->title }}" type="text" class="form-control" name="title"
                                    disabled>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input value="{{ $post->description }}" type="text" class="form-control"
                                    name="description" disabled>
                            </div>
                            <div class="form-group">
                                <label for="body">Body</label>
                                <input value="{{ $post->body }}" type="text" class="form-control" name="body"
                                    disabled>
                            </div>

                            <div class="form-group">
                                <img src="/img/default.jpg" class="img-fluid rounded-start" alt="...">
                            </div>

                        </div> --}}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <div class="col-12">
                                        <img src="{{ asset('storage/' . $post->sampul) }}" class="product-image"
                                            style="width: 300px" alt="Product Image">
                                    </div>
                                    <div class="col-12 product-image-thumbs">
                                        <div class="product-image-thumb active"><img
                                                src="{{ asset('storage/' . $post->sampul) }}" alt="Product Image">
                                        </div>
                                        <div class="product-image-thumb"><img
                                                src="{{ asset('storage/' . $post->sampul) }}" alt="Product Image">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <h3 class="my-3">{{ $post->title }}</h3>
                                    <p>Lorem Ipsum</p>
                                    <hr>
                                </div>
                                <div class="row mt-4">
                                    <nav class="w-100">
                                        <div class="nav nav-tabs" id="product-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab"
                                                href="#product-desc" role="tab" aria-controls="product-desc"
                                                aria-selected="true">Description</a>
                                            <a class="nav-item nav-link" id="product-comments-tab" data-toggle="tab"
                                                href="#product-comments" role="tab" aria-controls="product-comments"
                                                aria-selected="false">Comments</a>
                                            <a class="nav-item nav-link" id="product-rating-tab" data-toggle="tab"
                                                href="#product-rating" role="tab" aria-controls="product-rating"
                                                aria-selected="false">Rating</a>
                                        </div>
                                    </nav>
                                    <div class="tab-content p-3" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="product-desc" role="tabpanel"
                                            aria-labelledby="product-desc-tab">{{ $post->body }} </div>
                                        <div class="tab-pane fade" id="product-comments" role="tabpanel"
                                            aria-labelledby="product-comments-tab">{{ $post->body }}</div>
                                        <div class="tab-pane fade" id="product-rating" role="tabpanel"
                                            aria-labelledby="product-rating-tab">{{ $post->body }} </div>
                                    </div>
                                </div>
                            </div>




                            <!-- /.card-body -->

                            <div class="card-footer">
                                <a href="{{ route('posts.index') }}" class="btn btn-default">Back</a>
                            </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.1.0
            </div>
            <strong>Copyright &copy; <?= date('Y') ?> <a href="#">Indoposting</a>.</strong> All rights
            reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="../../adminlte/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="../../adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../adminlte/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../adminlte/dist/js/demo.js"></script>
    <script src="../../adminlte/plugins/datatables/jquery.dataTables.js"></script>
    <script src="../../adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


    @stack('script')
</body>

</html>
