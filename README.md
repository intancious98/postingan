## Yang saya kerjakan 
1. membuat role dan permission menggunakan spatie laravel
2. membuat tampilan admin menggunakan template AdminLTE
3. membuat migration dan seeder
4. membuat postingan untuk role Admin
5. membuat bahasa indonesia dan bahasa inggris (pending)
6. management user
7. management role
8. menu post (create, read, update, delete)
## Perubahan pada file :
1. routes web.php
2. controller,model, view
3. middleware Permission
4. resources/lang/en dan id
5. storage

## Step menjalankan project :
1. git clone 
2. buat file .env
3. buat db dengan nama 'postingan'
5. setting .env
4. jalankan **php artisan key:generate** pada terminal
5. lakukan migration **php artisan migrate** pada terminal
6. lakukan seeding data **php artisan db:seed --class=CreateAdminUserSeeder** pada terminal
7. jalankan composer update
8. jalankan **php artisan serve**
9. login dengan 
*email : admin@gmail.com*
*password : admin123*

#Flutter Mobile
saya mengerjakan pembuatan api, memperbaiki beberapa kesalahan di web, hosting web dan membuat widged login pada flutter, untuk hosting webnya dapat dilihat http://postingan-online.herokuapp.com/

Download APK   https://drive.google.com/file/d/19i_X2B261zQEcBfwK3xJhCQ6t7SxB0rY/view?usp=sharing
![Capture_login](/uploads/ade4d9a7da33fdddac03bd3dfc8e5ee1/Capture_login.PNG)













